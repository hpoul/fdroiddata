Categories:Reading
License:GPL-3.0-or-later
Author Name:Paul Hill
Web Site:
Source Code:https://github.com/zikalify/StoicReading
Issue Tracker:https://github.com/zikalify/StoicReading/issues

Auto Name:Stoic Reading
Summary:Consolidation of Stoic texts
Description:
This app contains texts from Stoics including those of Emperor Marcus Aurelius,
the freed slave Epictetus, and Seneca. It contains other, less well known, Stoic
texts too.
.

Repo Type:git
Repo:https://github.com/zikalify/StoicReading

Build:0.7.1,4
    commit=0.7.1
    subdir=app
    gradle=yes

Build:0.8.0,5
    commit=0.8.0
    subdir=app
    gradle=yes

Build:0.8.2,7
    commit=0.8.2
    subdir=app
    gradle=yes

Build:0.8.3,8
    commit=0.8.3
    subdir=app
    gradle=yes

Build:0.8.5,10
    commit=0.8.5
    subdir=app
    gradle=yes

Build:0.8.6,11
    commit=0.8.6
    subdir=app
    gradle=yes

Build:0.8.7,12
    commit=0.8.7
    subdir=app
    gradle=yes

Build:0.9.0,15
    commit=0.9.0
    subdir=app
    gradle=yes

Build:0.9.4,19
    commit=0.9.4
    subdir=app
    gradle=yes

Build:0.9.6,21
    commit=0.9.6
    subdir=app
    gradle=yes

Build:0.9.8,23
    commit=0.9.8
    subdir=app
    gradle=yes

Build:0.9.10,25
    commit=0.9.10
    subdir=app
    gradle=yes

Build:0.9.11,26
    commit=0.9.11
    subdir=app
    gradle=yes

Build:0.9.12,27
    commit=0.9.12
    subdir=app
    gradle=yes

Build:0.9.13,28
    commit=0.9.13
    subdir=app
    gradle=yes

Build:0.9.14,29
    commit=0.9.14
    subdir=app
    gradle=yes

Build:0.9.15,30
    commit=0.9.15
    subdir=app
    gradle=yes

Build:1.0.0,31
    commit=1.0.0
    subdir=app
    gradle=yes

Build:1.0.1,32
    commit=1.0.1
    subdir=app
    gradle=yes

Build:1.0.3,34
    commit=1.0.3
    subdir=app
    gradle=yes

Build:1.1.0,35
    commit=1.1.0
    subdir=app
    gradle=yes

Build:1.1.1,36
    commit=1.1.1
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags ^[0-9.]+$
Current Version:1.1.1
Current Version Code:36
